= SPL Iterators

== Tutorials on Spl Iterators

* link:https://evolvingweb.com/blog/iterating-over-trees-php[Iterating over trees in PHP]
looked at already. Mentions `iterator_to_array()` function.
* Excellent example code for all link:https://gist.github.com/hakre/3599532[PHP iterator types].
* Excellent link:https://dev.to/gbhorwood/php-doing-recursion-with-recursive-iteratoriterators-fj1[php: doing recursion with recursive iterator(iterator)s]
* link:https://stackoverflow.com/questions/19709410/php-recursive-multidimension-array-iterator[Array Recursive Iteration]
* link:https://resources.oreilly.com/examples/9781786463890[Book on PHP Data Structs]

