<?php
/*
If you are iterating over a multi-dimensional array of objects, you may be tempted to use a `RecursiveArrayIterator` within a `RecursiveIteratorIterator`.
You are likely to get baffling results if you do. That is because `RecursiveArrayIterator` treats all objects as having children, and tries to recurse
into them. But if you are interested in having your `RecursiveIteratorIterator` return the objects in your multi-dimensional array, then you don't
want the default setting `LEAVES_ONLY`, because no object can be a leaf (= has no children).

The solution is to extend the `RecursiveArrayIterator` class and override the `hasChildren()` method appropriately. Something like the following might
be suitable:
*/

$myArray = [
    0 => 'depth 1, first element',

    1 => ['Subarray: depth 2: first element',
           [ 
             0 => 'Subarray of subarray: depth 3: first element',
             1 => 'Subarray of subarray: depth 3: second element',
             2 => [
                   'Subarray of subarray of subarray: depth 4: first element',
                   'Subarray of subarray of subarray: depth 4: second element'
                   ]
            ]
        ],

    2 => 'depth 1: second element',

    3 => ['depth 2: first element', 'depth 2: second element'],

    4 => 'c'
];


class RecursiveArrayOnlyIterator extends RecursiveArrayIterator {

    public function __construct(array $array = [], int $flags = 0)
    {
        parent::__construct($array, $flags);
    }
  public function hasChildren() : bool
  {
    if (\is_array( $this->current() ) ) {

        echo "this->current() is an array.\n";
    }

    return parent::hasChildren(); 
  }

  public function getChildren(): ?RecursiveArrayIterator
  {
     echo "Getting Children\n";

     return parent::getChildren();
  }
}

$iter1 = new  RecursiveArrayOnlyIterator($myArray); 

foreach ($myArray as $key => $value) {
  
  echo "key = $key. value = $value.\n";
  
}

/*
Of course, this simple example will not recurse into ArrayObjects either!
*/

/* Example 2

Using the `RecursiveArrayIterator` to traverse an unknown amount of sub arrays within the outer array. Note: This functionality is already provided by
using the `RecursiveIteratorIterator` but is useful in understanding how to use the iterator when using for the first time as all the terminology
does get rather confusing at first sight of SPL!
*/


$iterator = new RecursiveArrayIterator($myArray);

iterator_apply($iterator, 'traverseStructure', array($iterator));

function traverseStructure($iterator) {

    while ( $iterator->valid() ) {

        if ( $iterator->hasChildren() )

            traverseStructure($iterator->getChildren());

        else 

            echo $iterator -> key() . ' : ' . $iterator->current() .PHP_EOL;    

        $iterator -> next();
    }
}

/*
The output from which is:

----
0 : a

0 : subA

1 : subB

0 : subsubA

1 : subsubB

0 : deepA

1 : deepB

2 : b

0 : subA

1 : subB

2 : subC

4 : c
----
*/

/*
Example 3 and Comments

*/
$array = [
'A','B',
'C'=>[
    'D','E',
    'F'=>['G','H']
 ],
'I','J'
];

$iterator = new RecursiveArrayIterator($array);

foreach($iterator as $key=>$value)
    echo $key .':' . $value . "\n";

/*

Output:

----
0:A
1:B
C:Array
2:I
3:J
----

Recursive approach...

*/

$array = [
'A','B',
'C'=>[
    'D','E',
    'F'=>['G','H']
 ],
'I','J'
];

$it = new RecursiveArrayIterator($array);
$iterator = new RecursiveIteratorIterator($it);

foreach($iterator as $key => $value) {

    echo "depth is: " . $iterator->getDepth() . "\n";
    echo $key,':', $value, "\n";
}

/*
The output is:

----
Output
0:A
1:B
0:D
1:E
0:G
1:H
2:I
3:J
*/
