<?php

error_reporting(E_ALL & ~E_WARNING);

function run_test(RecursiveArrayIterator $ait, int $mode = RecursiveIteratorIterator::LEAVES_ONLY)
{
  $it = new RecursiveIteratorIterator($ait, $mode);

  foreach ($it as $key => $leaf) {

      $depth = $it->getDepth();

      echo "[Depth: $depth] ";

      while ($depth-- > 0)
         echo "\t";

      echo "key: $key => leaf: $leaf", PHP_EOL;
  }
}

function tests(array $a)
{
  $ait = new RecursiveArrayIterator($a);

  echo "Printing the array:\n";

  print_r($a);

  echo "\nThe mode is LEAVES_ONLY. Parents are ignored.\n\n";

  run_test($ait, RecursiveIteratorIterator::LEAVES_ONLY);

  echo "\nThe mode is SELF_FIRST: parents come before children.\n\n";

  run_test($ait, RecursiveIteratorIterator::SELF_FIRST);

  echo "\nThe mode is CHILD_FIRST: children come before parents.\n\n";

  run_test($ait, RecursiveIteratorIterator::CHILD_FIRST);
}

$array = [
 'A',
 'B',
 'C' => [
     'D',
     'E',
     'F' => [
         'G',
         'H'
        ]
     ],
 'I',
 'J'
];

tests($array);

$array = array(
    array(
        array(
            array(
                'leaf-0-0-0-0',
                'leaf-0-0-0-1'
            ),
            'leaf-0-0-0'
        ),
        array(
            array(
                'leaf-0-1-0-0',
                'leaf-0-1-0-1'
            ),
            'leaf-0-1-0'
        ),
        'leaf-0-0'
    )
);

echo "\n------------------\n";

tests($array);

