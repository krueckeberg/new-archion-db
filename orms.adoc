= Orms

== Orm Implementations

* link:https://southprojects.com/using-orm-in-php-without-a-framework[using-orm-in-php-without-a-framework]
* link:https://codereview.stackexchange.com/questions/169345/building-a-simple-orm-in-php[building-a-simple-orm-in-php]
* link:https://www.youtube.com/watch?v=nsy-5cJcPdA[Video on Creating Your Own ORM]
* link:https://medium.com/@.Chromax/building-an-orm-using-pdo-abstraction-layer-in-php-db685f84eb9e[building-an-orm-using-pdo-abstraction-layer-in-php-db685f84eb9e[]
