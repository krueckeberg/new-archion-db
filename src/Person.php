<?php
declare(strict_types=1);
namespace Archion;

readonly class Person {

  public string $given;
  public string $surname;
  public string $gender;

  public Event $event;

  public \ArrayObject $facts; 
 
  function __construct(array $person, Event $event)
  {
     $this->given = $person['given']; 
     
     $this->surname = $person['surname']; 
     
     $this->gender = $person['gender']; 
 
     $this->event = $event;
     
     $this->facts = new \ArrayObject;
     
     foreach($person['facts'] as $key => $fact) {
    
        $this->facts[$key] = new Fact($fact, $this);  // TODO: Do I need to pass $this?     
    } 
  }

  function __toString() : string
  {
   $str = "Given: " .  $person['given']; 
   
   $str .= "Surname: " . $person['surname']; 
   
   $str .= "Gender: " . $person['gender']; 

   $str .= "----------------------------\n";

   return $str;
 
  }
}
