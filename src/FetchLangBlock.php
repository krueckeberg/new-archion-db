<?php
declare(strict_types=1);
namespace Archion;

class FetchLangBlock {

   private string $path;

   private static $patterns = [ "/^([^;]+)/", "/(;)$/"];

   private static $replacements = [ "$1", "$1\n"];

   private string $language;
 
   function __construct(string $subdir, string $language)
   {
       $this->path = $subdir;
       $this->language = $language;  
   }

   function process_file(string $filename) : array
   {
     $in = new \SplFileObject($this->path . $filename, "r");
   
     $in->setFlags(\SplFileObject::READ_AHEAD|\SplFileObject::DROP_NEW_LINE);
   
     return $this->get_block($in, $this->language);
   }

   function get_block(\SplFileObject $in, string $language) : array
   {
     $processing_block = false;
   
     $is_sql = false;
   
     $result = [];

     $source_long = "[source,$language]";

     $source_short = "[,$language]";
   
     foreach($in as $line) {
   
        if ((strpos($line, $source_long) === 0) || (strpos($line, $source_short) === 0)) {
   
             $is_sql = true;
        }        

        $flag = (strpos($line, "----") === 0) && $is_sql;
            
        if ($flag && $processing_block) {
    
              $is_sql = $processing_block = false;
     
        } elseif ($flag)  { 
            
             $processing_block = true;
             continue;
        }
        
        if ($processing_block === true) {

             $output = preg_replace(self::$patterns, self::$replacements, $line);

             echo "{$output}\n";

             $result[] = $output;
        }
     }
     
     return $result;
   }

   function __invoke(string $filename) : array
   {
      return $this->process_file($filename);
   }
}
