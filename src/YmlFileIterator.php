<?php
declare(strict_types=1);
namespace Archion;

class YmlFileIterator implements \Iterator {

   private int    $index;
   private array  $subdirs;
   private string $start_dir;
   private int    $last_index;
   
   private \FileSystemIterator $cur_iter;

   function __construct(array $config)
   {
      $this->start_dir = $config['start_dir'];

      $this->subdirs = $config['sub_dirs'];
  
      $this->last_index = \count($this->subdirs) - 1;
      
      $this->index = 0;

      $this->cur_iter = new \FilesystemIterator($this->build_path($this->index));
   }

   private function build_path(int $i) : string
   {
      $path = $this->start_dir . $this->subdirs[$i] . "/yml";
      
      return $path;
   }

   function rewind() : void
   {
      if ($this->index != 0) { 

         $this->index = 0;

         $this->cur_iter = new \FilesystemIterator($this->build_path($this->index));
      } 

      $this->cur_iter->rewind();
   }
  
   private function next_valid_iterator() : void 
   {
      // Return the next valid iterator, of a subdir with actual .yml files, if one exists.
      for (++$this->index; $this->index <= $this->last_index && $this->cur_iter->valid() == false; ++$this->index) {
              
          $this->cur_iter = new \FilesystemIterator($this->build_path($this->index));
      }       
   }

   function valid() : bool
   {
      if ($this->cur_iter->valid()) { // If the current iterator is still valid, return true.

          return true;

        // Return false if we are iterating the last subdir in $this->subdirs--since it is no longer valid.
      } else if ($this->index == $this->last_index) { 

          return false;    

      } else {

          $this->next_valid_iterator();
          
          return $this->cur_iter->valid(); 
      }
   }

   function next() : void
   {       
     $this->cur_iter->next();
   }

   function current() : \SplFileInfo
   {
      return $this->cur_iter->current();
   }

   function key() : string
   {
      return $this->cur_iter->key();
   }
}
  
