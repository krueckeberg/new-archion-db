<?php
declare(strict_types=1);
namespace Archion;

class DebugFilesystemIterator extends \FileSystemIterator {


    function __construct(string $directory,
             int $flags = \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS)
    {
       parent::__construct($directory, $flags);
    }  


    function current() : string|\SplFileInfo|\FilesystemIterator
    {
      echo "-------------\n";

      $c = parent::current();

      echo "In current(): \n";
      echo "\tisDir() = ";
      var_dump($c->isDir());

      echo "\tisFile() = ";
      var_dump($c->isFile());

      echo "\tgetFilename() = " . $c->getFilename() . "\n";
      echo "\tgetPath() = " . $c->getPath() . "\n";

      return parent::current();
    }

    function key() : string
    {
       $k = parent::key();

       echo "In key(): key() = ";

       var_dump($k);
       
       return parent::key();
    }
    
    function rewind() : void
    {
       echo "In rewind():\n";
   
    }

    function next() : void
    {
      echo "In next():\n";
      parent::next(); 
    }

    function valid() : bool
    {
       $b = parent::valid();

       echo "In valid(): ";
       var_dump($b);         

       return $b;
    }
}
