<?php
declare(strict_types=1);

function get_yaml_file(string $yml_filename)
{
   $array = \yaml_parse_file($yml_filename);

   print_r($array); 
}

function test2($yml)
{
   $aiter = new RecursiveArrayIterator($yml);
   $iter = new RecursiveIteratorIterator($aiter);

   foreach($iter as $key => $value) {

         echo "Key = $key\n";

         echo "value is => \n";

         var_dump($value);

         echo "------------\n";
   }
   
}

if ($argc !== 2)
   die("yml filename not given.\n");

else if (!file_exists($argv[1])) {

   die("File {$argv[1]} does not exist.");
}

get_yaml_file($argv[1]);
