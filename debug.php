<?php
declare(strict_types=1);
use Archion\{DebugFilesystemIterator, Config};

include "vendor/autoload.php";

$c = new Config;

$config = $c->configuration;

$subdirs = $config['sub_dirs'];

for($i = 0; $i < count($subdirs); ++$i) {

     $dir = $config['start_dir'] . $subdirs[$i] . "/yml";   

     $iter = new DebugFilesystemIterator($dir);

     try {  

        echo "\nIterating over \$subdirs[$i] = '{$subdirs[$i]}' with DebugFilesystemIterator('$dir'):\n\n";

        foreach ($iter as $key => $fileInfo) {

        }

     } catch(\Exception $e) {

         echo $e->getMessage() . "\n";

         echo "Error occurred at " . $e->getFile() . " on line " . $e->getLine();
         return;
     } 
}
