<?php
declare(strict_types=1);
use Archion\{YmlFileIterator, Config};

include "vendor/autoload.php";

$c = new Config;

$config = $c->configuration;

$iter = new YmlFileIterator($config);

foreach($iter as $key => $value) {
     
     try {  
        echo "key: $key\n";
        echo "current: " . $value->getPathname() . "\n";

     } catch(\Exception $e) {

         echo $e->getMessage() . "\n";
         echo "Error occurred at " . $e->getFile() . " on line " . $e->getLine();
         return;
     } 
}
